GUADEC talk notes
===

Abstract
---

How environmentally friendly is GNOME, both the software and the project? What can we do to make it better, at a technical level and at a social level? This is going to be a talk of observations, and maybe some thoughts about solutions, but I don’t have all the answers.

Ideas
---

 * Reduce power consumption
 * Use batteries to smooth out grid demand; charge when electricity is low-CO2-equiv
  * Some EFIs now have options for this; typically not exposed to the kernel
 * Reduce the need for throwaway/new hardware
 * Reduce bandwidth consumption
 * Make fewer flights (especially transatlantic ones)
  * Compare GNOME’s flight usage vs a company of the same size
 * Promote local developer groups
 * Profile cost of CI infrastructure
  * Is it running on renewable power?
 * Smart home integration?
 * Lots more social opportunities for GNOME on a phone (recipes, food purchasing to reduce waste, day-to-day transport optimisation) — these are all apps outside the scope of GNOME
 * Foundation-level decisions:
  * Discouraging travel
  * Cutting down on printing
  * Environmentally friendly purchasing decisions
  * Environmentally friendly investment
	At the moment, we have a current account and a savings account, both
	held with BBVA Compass. The savings account is their Business Money
	Market one, and I can't find specific information in how this is
	managed.

	However, one of their four key priorities is for:
	"Sustainable finance to combat climate change, respect
	human rights and achieve the UN Sustainable Development
	Goals (SDGs)."

	More details about what they've signed up to is: 
	https://www.bbva.com/en/bbva-endorses-the-united-nations-principles-for-responsible-banking/
	and https://www.unepfi.org/banking/bankingprinciples/

	Neil
  * Need to discuss this all with the board before presenting

Ideas from while walking
---

 * Run slides past the board
 * Quantise transport emissions
 * Quantise server emissions
  - Big single change: stop downloading dependencies on every CI run
  - Network usage
  - Processor usage
 * Quantise desktop emissions over 1 million(?) users
 * Provide background/motivation on CO2 emissions for context
 * Suggestions/Tools for how to improve software performance
 * Start of a discussion --- I don’t have all the answers
 * Citations and further reading
 * Offer hope
 * What are our CO2 emission externalities?
 * Reduce/Discourage consumerism
 * What’s the Foundation doing?
 * Suggestions:
  - Hold GUADEC every other year
  - More, smaller, local hackfests
  - Work remotely
  - Offsets are not an option
  - CO2 budget for each event (or annually)
  - GUADEC/Hackfest organisers push for institutional change in hosts => buses, drinks cups, veggie food
  - Train travel within Europe
  - Remote presence and participation
  - Can our servers be renewably powered?
  - All lower your own footprints, and those you can influence
   * These are externalities to GNOME
   * Talk to me afterwards about more general efforts

Research questions:
 * How often *can* I fly transatlantic and keep within CO2 budget?
 * Conference remote presence options
 * CI server emissions
 * Internet traffic emissions
 * Number of GNOME users
